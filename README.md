# Spring-5-WebFlux
Spring 5 WebFlux demo

## Things to do
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-webflux.git`
2. Go to the folder: `cd springboot-webflux`
3. Run the application: `mvn clean spring-boot:run`. 

(We assume that You already installed JDK8, Maven & MongoDB on your system)
