package com.hendisantika.springbootwebflux.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-webflux2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/11/19
 * Time: 21.53
 */
public class TweetNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TweetNotFoundException(String tweetId) {
        super("Tweet not found with id " + tweetId);
    }
}