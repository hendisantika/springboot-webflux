package com.hendisantika.springbootwebflux.payload;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-webflux2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/11/19
 * Time: 21.52
 */
public class ErrorResponse {
    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
